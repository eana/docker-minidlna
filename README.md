# DNLA server in Docker

Small DLNA server thanks to MiniDLNA under Alpine Linux.

## Build
```
docker build -t minidlna github.com/eana/docker-minidlna
```

## Usage

Quite simple to launch :)

However, the intention here is to use the server over a VPN tunnel (OpenVPN in my case).
To make it work, we use the host's network to bind `minidlnad` to the `tun0` interface.

Interactive mode:

```
docker run --name minidlna --net=host -it --rm -v /var/cache/minidlna:/cache -v YOUR_DIR_ON_HOST_1:/media1 -v YOUR_DIR_ON_HOST_2:/media2 -v YOUR_DIR_ON_HOST_3:/media3 minidlna
```

Detached mode:

```
docker run --name minidlna --net=host -d -v /var/cache/minidlna:/cache -v YOUR_DIR_ON_HOST_1:/media1 -v YOUR_DIR_ON_HOST_2:/media2 -v YOUR_DIR_ON_HOST_3:/media3 minidlna
```

_Note: you can add up to 5 media dirs_
