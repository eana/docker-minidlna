FROM alpine:3.5

RUN set -xe && \
    apk add --update --no-cache minidlna && \
    sed -e "s/#network_interface=eth0/network_interface=tun0/g" -i /etc/minidlna.conf && \
    sed -e "s/media_dir=\/opt/media_dir=\/media1\nmedia_dir=\/media2\nmedia_dir=\/media3\nmedia_dir=\/media4\nmedia_dir=\/media5/g" -i /etc/minidlna.conf && \
    sed -e "s/^#db_dir=.*/db_dir=\/cache/g" -i /etc/minidlna.conf && \
    sed -e "s/^#friendly_name.*/friendly_name=Jonas\'s DLNA server/g" -i /etc/minidlna.conf && \
    for count in $(seq 1 5); do mkdir -vp /media${count}; done

CMD minidlnad -d
